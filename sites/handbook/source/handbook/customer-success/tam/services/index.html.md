---
layout: handbook-page-toc
title: "TAM Responsibilities and Services"
description: "There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## TAM Alignment

Customers who meet the following criteria are aligned with a Technical Account Manager.  *These criteria are reviewed annually and are subject to change*:

### Enterprise

- [Meets Enterprise Segment Criteria](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)
- Recurring contract value (ARR) of $50,000 or greater
- Exceptions can be made for clearly defined growth opportunities, decided by sales and TAM leadership

#### Parent-Child Accounts and Business Units

It's not uncommon for a single customer to have different subsidiaries or [business units](/handbook/sales/sales-term-glossary/#business-unit) with their own GitLab instances and contracts. In effect, each of these separate entities behaves as a separate customer from the perspective of the TAM - different points of contact, different initiatives, different strategic outcomes.

When evaluating whether a customer is TAM qualifying, we consider the size of the [business unit](/handbook/sales/sales-term-glossary/#business-unit), not the total ARR of a given customer, nor the total ARR of a parent account and all of their combined child accounts.

Example:

- `Customer A` has $50k ARR spread across three separate [business units](/handbook/sales/sales-term-glossary/#business-unit) spending $22k, $17k, and $10k respectively. None of those is TAM qualifying.
- `Customer B` has $60k ARR spread across two [business units](/handbook/sales/sales-term-glossary/#business-unit) spending $53k and $7k respectively. The [business unit](/handbook/sales/sales-term-glossary/#business-unit) spending $53k is TAM qualifying, but the other is not.

### Commercial

- Commercial (Mid-Market and SMB) customers are eligible for a TAM if the recurring contract value (ARR) is greater than $50,000.
- Exceptions can be made for clearly defined growth opportunities, decided by sales and TAM leadership. The basic criteria used will be (1 AND 2 AND 3):
    1. Well defined growth opportunity
    1. Clear objectives/goals - what do they need TAM help to accomplish?
    1. Timeframe - if we accomplish X and Y in Z months, we agree to evaluate expanding/growing
- [Commercial TAM group direction](/handbook/customer-success/tam/commercial/vision/)

## Responsibilities and Services

There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilization of GitLab's products and services. These services typically include the following. Please note this list is not definitive, and more services may be provided than listed or some may not be offered, depending on the size and details of the account.

### Relationship Management

- Regular cadence calls
  - Ask customers about planned upgrades on a regular basis. Refer them to the [Live Upgrade Assistance page](/support/scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance) so that they have a plan in place (including rollback strategy) and give Support enough preparation time to be available to help.
- Regular open issue reviews and issue escalations
- Account health checks
- Executive business reviews (1-2 times a year for Enterprise; as needed for Commercial)
- Success strategy roadmaps - beginning with an onboarding success plan, for example a 30/60/90 day plan
- To act as a key point of contact for guidance and advice and as a liaison between the customer and other GitLab teams
- Own, manage, and deliver the customer onboarding experience
- Help GitLab's customers realize the value of their investment in GitLab
- GitLab Days

### Training

- Identification of pain points and training required
- Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
- "Brown Bag" trainings
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

### Support

- Upgrade planning
- User adoption strategy
- Migration strategy and planning
- [Infrastructure upgrade coordination](/handbook/customer-success/tam/services/infrastructure-upgrade/)
- Launch support
- Monitors support tickets and ensures that the customer receives the appropriate support levels
- Support ticket escalations
- Monitor SaaS based customer experience by adding them to the [Marquee Accounts alerts](https://gitlab.com/gitlab-com/gl-infra/marquee-account-alerts) project
